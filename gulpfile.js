import gulp from 'gulp';
import concat from 'gulp-concat';
import clean from 'gulp-clean';
import autoprefixer from 'gulp-autoprefixer';
import BS from 'browser-sync';
const browserSync = BS.create();
const { src, dest, task, watch, series } = gulp;
import gulpSass from 'gulp-sass';
import nodeSass from 'sass';
const sass = gulpSass(nodeSass);
import purgecss from 'gulp-purgecss';
import minify from 'gulp-minify';
import imagemin from 'gulp-imagemin';

const imgMin = () => gulp.src('src/img/**/*')
		.pipe(imagemin())
		.pipe(gulp.dest('./dist/img'));


const cleanDist = () => gulp.src('dist/*', {read: true})
            .pipe(clean(['dist/**/*', '!dist/img/*']));


const buildCssFunk = () => gulp.src('src/scss/style-min.scss')
            .pipe(concat('styles.min.css'))
            .pipe(sass().on('error', sass.logError))
            .pipe(autoprefixer({cascade: false}))
            .pipe(purgecss({content: ['src/**/*.html']}))
            .pipe(gulp.dest('./dist/css'));

const buildHtml = () => gulp.src("src/index.html")
            .pipe(gulp.dest("dist"))

const builsJsFunk = () => gulp.src('src/scripts/*')
            .pipe(concat('styles.min.js'))
            .pipe(minify())
            .pipe(gulp.dest('./dist/js'));


const startWatching = () => {
    browserSync.init({
        server: {
            baseDir: "./dist",
            // index: "index.html",
            // directory: true
        }
    });
    watch('src/**/*').on('all', series(buildCssFunk, buildHtml, browserSync.reload));
    // watch('src/styles/**/*').on('all', series(convertCss, browserSync.reload));
}
            

gulp.task("imgMin", imgMin);
gulp.task("build", gulp.series(cleanDist, gulp.parallel(buildHtml, buildCssFunk, builsJsFunk, imgMin)));
gulp.task("dev", gulp.series(startWatching));











